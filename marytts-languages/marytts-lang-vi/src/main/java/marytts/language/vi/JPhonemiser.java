package marytts.language.vi;

import marytts.datatypes.MaryData;
import marytts.datatypes.MaryDataType;
import marytts.exceptions.MaryConfigurationException;
import marytts.modules.InternalModule;
import marytts.modules.phonemiser.AllophoneSet;
import marytts.server.MaryProperties;
import marytts.util.MaryRuntimeUtils;

import java.io.*;
import java.util.*;

/**
 * Created by hieu on 07/12/2015.
 */
public class JPhonemiser extends InternalModule {
    Map<String, List<String>> userdict;
    AllophoneSet allophoneSet;

    public JPhonemiser(String propertyPrefix) throws IOException, MaryConfigurationException {
        this("JPhonemiser", MaryDataType.PARTSOFSPEECH, MaryDataType.PHONEMES, propertyPrefix + "allophoneset",
                propertyPrefix + "userdict");
    }

    public JPhonemiser(String name, MaryDataType inputType, MaryDataType outputType, String allophonesProperty, String userdictProperty) throws MaryConfigurationException, IOException {
        super(name, inputType, outputType, MaryRuntimeUtils.needAllophoneSet(allophonesProperty).getLocale());
        allophoneSet = MaryRuntimeUtils.needAllophoneSet(allophonesProperty);
        String userdictFilename = MaryProperties.getFilename(userdictProperty);
        if (userdictFilename != null) {
            if (new File(userdictFilename).exists()) {
                userdict = readLexicon(userdictFilename);
            } else {
                logger.info("User dictionary '" + userdictFilename + "' for locale '" + getLocale()
                        + "' does not exist. Ignoring.");
            }
        }

    }

    protected Map<String, List<String>> readLexicon(String lexiconFilename) throws IOException {
        String line;
        Map<String, List<String>> fLexicon = new HashMap<String, List<String>>();

        BufferedReader lexiconFile = new BufferedReader(new InputStreamReader(new FileInputStream(lexiconFilename), "UTF-8"));
        while ((line = lexiconFile.readLine()) != null) {
            // Ignore empty lines and comments:
            if (line.trim().equals("") || line.startsWith("#"))
                continue;

            String[] lineParts = line.split("\\s*\\|\\s*");
            String graphStr = lineParts[0];
            String phonStr = lineParts[1];
            try {
                allophoneSet.splitIntoAllophones(phonStr);
            } catch (RuntimeException re) {
                logger.warn("Lexicon '" + lexiconFilename + "': invalid entry for '" + graphStr + "'", re);
            }
            String phonPosStr = phonStr;
            if (lineParts.length > 2) {
                String pos = lineParts[2];
                if (!pos.trim().equals(""))
                    phonPosStr += "|" + pos;
            }

            List<String> transcriptions = fLexicon.get(graphStr);
            if (null == transcriptions) {
                transcriptions = new ArrayList<String>();
                fLexicon.put(graphStr, transcriptions);
            }
            transcriptions.add(phonPosStr);
        }
        lexiconFile.close();
        return fLexicon;
    }

    @Override
    public MaryData process(MaryData d) throws Exception {
        return super.process(d);

    }
}
